<?php
/***************************************************************
 ***  CAS Maestro functions file 							 ***
 ***************************************************************/

/**
 * removeEmptyItems
 * To be used with array_filter
 * @param &$item passed by reference, the array item being tested.
 */
function removeEmptyItems(&$item) {
	//Verify is the
    if (is_array($item) && $item) {
        $item = array_filter($item, 'removeEmptyItems');
    }

    return !empty($item);
}

/**
 * Check for a empty value and if there is an error defined
 * and print a class.
 */
function check_empty($variable) {
	if(empty($variable) && isset($_GET['error'])) {
		echo "class='required_field'";
	}
}

/**
 * Retrieve the WordPress user_id by the corresponding matching "cas_userid"
 * meta key, or false if one could not be found.
 */
function get_user_by_cas_userid($cas_userid) {
	global $wpdb;
	if (!$user_id = $wpdb->get_var($wpdb->prepare(
		"SELECT user_id FROM $wpdb->usermeta " .
		'WHERE meta_key = \'%s\' AND meta_value = \'%s\' LIMIT 1;',
		"cas_userid",
		strval($cas_userid)
	))) {
		return false;
	}
	return intval($user_id);
}
